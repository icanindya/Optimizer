import java.util.Arrays;
import ilog.concert.*;
import ilog.cplex.*;

public class Optimizer1 {
	
	public static void optimize(){
		
		//parameters
		// number of clusters
		int k = 4;
		
		// number of features
		int n = 5;
		
		// cluster centers
		int clusters[][] = {
				{10, 5, 2, 9, 0},
				{3, 2, 8, 2, 9},
				{7, 6, 9, 9, 2},
				{1, 10, 0, 1, 8},
				};
		
		// attack point
		double a[] = {5, 10, 1, 2, 9};
		
		// distance threshold
		double t = 4;
		
		// feature modification thresholds
		double tF[] = {9, 9, 9, 9, 9};
		
		int currCluster = -1;
		double currObjVal = Double.MAX_VALUE;
		double currAP[] = new double[n];;
		
		
		try {
			
			for(int j = 0; j < k; j++){
				// define new model
				
				IloCplex cplex = new IloCplex(); 
				
				//variables
				
				//modified attack point
				IloNumVar[] aP = cplex.numVarArray(n, 0, Double.MAX_VALUE);			
				
				//objective
		
				IloNumExpr objective =  cplex.numExpr();
				IloNumExpr numExprs[] = new IloNumExpr[n];
				
				for(int i = 0; i < n; i++){
					numExprs[i] = cplex.square(cplex.diff(aP[i], a[i]));
				}
				
				objective = cplex.sum(numExprs);
				cplex.addMinimize(objective);
				
				//constraints
				
				//constraint 1
				IloNumExpr numExprs2[] = new IloNumExpr[n];
				
				for(int i = 0;  i < n; i++){
					numExprs2[i] = cplex.square(cplex.diff(aP[i], clusters[j][i]));
				}
				
				cplex.addLe(cplex.sum(numExprs2), Math.pow(t, 2));
				
				//constraint 2
				for(int i = 0; i < n; i++){
					cplex.addLe(cplex.square(cplex.diff(aP[i], a[i])), Math.pow(tF[i], 2));
				}
				
				System.out.println("\nCluster " + j + ": \n");
				
				if(cplex.solve()){
					
					System.out.println();
					System.out.println("Objective value: " + cplex.getObjValue());
					System.out.println("Target cluster: " + Arrays.toString(clusters[0]));
					System.out.println("Attack point: " + Arrays.toString(a));
					System.out.println("Modified attack point: " + Arrays.toString(cplex.getValues(aP)));
					
					if(cplex.getObjValue() < currObjVal){
						currCluster = j;
						currObjVal = cplex.getObjValue();
						currAP = cplex.getValues(aP);
					}
					
				}
				else{
					System.out.println();
					System.out.println("Couldn't solve the problem for cluster " + j + ".");
				}
				
			}
			
			System.out.println("\nResult: ");
			
			if(currCluster != -1){
				System.out.println("Attack point: " + Arrays.toString(a));
				System.out.println("Optimum cluster = " + currCluster + ": " + Arrays.toString(clusters[currCluster]));
				System.out.println("Modified attack point: " + Arrays.toString(currAP));
			}
			else{
				System.out.println("Solution not found.");
			}
			
		} catch (IloException e) {
			e.printStackTrace();
		}  
		
	}
	
}
